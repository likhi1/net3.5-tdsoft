﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//---- NameSpace Connect -------------------//
using System.Data;
using System.Data.SqlClient;
using System.Configuration;


/// <summary>
/// Summary description for HelperClass
/// </summary>
 

//Helper class   for use App setting  
public static class ConfigPath
{
    //===================== Set Website  Path 
    public static string SiteUrl {
        get { return ConfigurationManager.AppSettings["siteUrl"]; }

    }

    public static string BackendUrl {
        get { return ConfigurationManager.AppSettings["backendUrl"]; }

    }
      

   
}//end class

  