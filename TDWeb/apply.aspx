<%@ Page Language="C#" MasterPageFile="~/MasterPageFront1.master" AutoEventWireup="true" CodeFile="apply.aspx.cs"
    Inherits="tdSoft.apply" Title="" culture="auto" meta:resourcekey="PageResource1" uiculture="auto" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script src="scripts/jquery-1.8.3.js" type="text/javascript"></script> 
    <link href="css/global.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">5

        function chkNull() {
            var _salary = $("#salary").val();
            var _RadioButtonList1 = $("#RadioButtonList1").val();

            if (_salary = "") {
                alert("Please fill Expected Salary");
            } else if (_RadioButtonList1 = "") {
            alert("Please fill Position Level");
            } else {
                return true;
            }

        } //end func 
    </script>

    <form id="form1" runat="server">
    <!-- Trace --->
    <hr />
    <!-- Control --->
    <div id="page-apply">
        <div class="page-title">
            Application Form
         </div>
        
        <div class="clear">
        </div>
        <div class="colLeft">
            <div class="row-section">
                <h2>
                    Expected Salary</h2>
                Salary :
                <asp:TextBox ID="salary" runat="server" CssClass="myClass" 
                    meta:resourcekey="salaryResource1"></asp:TextBox>
                &nbsp; Baht
            </div>
            <!-- end class="row-section"  -->
            <div class="row-section" id="chkOne">
                <h2>
                    Position Leve l</h2>
                <asp:RadioButtonList ID="RadioButtonList1" runat="server" RepeatColumns="3" RepeatDirection="Horizontal"
                    Width="490px" meta:resourcekey="RadioButtonList1Resource1">
                    <asp:ListItem Selected="True" meta:resourcekey="ListItemResource1">Junior</asp:ListItem>
                    <asp:ListItem meta:resourcekey="ListItemResource2">Senior</asp:ListItem>
                    <asp:ListItem meta:resourcekey="ListItemResource3">Expert</asp:ListItem>
                </asp:RadioButtonList>
            </div>
            <!-- end class="row-section"  -->
            <div class="row-section">
                <h2>
                    Position Required</h2>
                <asp:CheckBoxList ID="CblPosition" runat="server" RepeatColumns="3" 
                    RepeatDirection="Horizontal" Width="490px" 
                    onselectedindexchanged="CblPosition_SelectedIndexChanged"     
                    AutoPostBack=True meta:resourcekey="CblPositionResource1" >
                    <asp:ListItem Value="System AnalysisValue" meta:resourcekey="ListItemResource4">System AnalysisText</asp:ListItem>
                    <asp:ListItem meta:resourcekey="ListItemResource5">Database Admin </asp:ListItem>
                    <asp:ListItem meta:resourcekey="ListItemResource6">Project Manager</asp:ListItem>
                    <asp:ListItem meta:resourcekey="ListItemResource7">QE/Tester</asp:ListItem>
                    <asp:ListItem meta:resourcekey="ListItemResource8">Web Design </asp:ListItem>
                    <asp:ListItem meta:resourcekey="ListItemResource9">Pogrammer</asp:ListItem>
                    <asp:ListItem meta:resourcekey="ListItemResource10">Other</asp:ListItem>
                </asp:CheckBoxList>
            </div>
            <!-- end class="row-section"  -->
            <div class="row-section">
                <h2>
                    Personal Info</h2>
                <table class="personal-info">
                    <tr>
                        <td class="label">
                            FirstName :
                        </td>
                        <td>
                            <asp:TextBox ID="nameF" runat="server" meta:resourcekey="nameFResource1"></asp:TextBox>
                            <span class="frmStar">*</span>
                        </td>
                        <td class="label">
                            Birth Date :
                        </td>
                        <td>
                            <asp:DropDownList ID="DdlBrtDay" runat="server" 
                                meta:resourcekey="DdlBrtDayResource1">
                            </asp:DropDownList>
                            <asp:DropDownList ID="DdlBrtMonth" runat="server" 
                                meta:resourcekey="DdlBrtMonthResource1">
                            </asp:DropDownList>
                            <asp:DropDownList ID="DdlBrtYear" runat="server" 
                                meta:resourcekey="DdlBrtYearResource1">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="label">
                            LastName :
                        </td>
                        <td>
                            <asp:TextBox ID="nameL" runat="server" meta:resourcekey="nameLResource1"></asp:TextBox><span class="frmStar"> *</span>
                        </td>
                        <td class="label">
                            Weight :
                        </td>
                        <td>
                            <asp:TextBox ID="weight" runat="server" meta:resourcekey="weightResource1"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="label">
                            Gen :
                        </td>
                        <td>
                            <asp:RadioButtonList ID="RadioButtonList2" runat="server" 
                                RepeatDirection="Horizontal" meta:resourcekey="RadioButtonList2Resource1">
                                <asp:ListItem Selected="True" meta:resourcekey="ListItemResource11">Male</asp:ListItem>
                                <asp:ListItem meta:resourcekey="ListItemResource12">Female</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                        <td class="label">
                            Height :
                        </td>
                        <td>
                            <asp:TextBox ID="height" runat="server" meta:resourcekey="heightResource1"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="label">
                            Age :
                        </td>
                        <td>
                            <asp:DropDownList ID="DdlAge" runat="server" 
                                meta:resourcekey="DdlAgeResource1">
                            </asp:DropDownList>
                            <span class="frmStar">*</span>
                        </td>
                        <td class="label">
                            Nationalty :
                        </td>
                        <td>
                            <asp:TextBox ID="nationalty" runat="server" 
                                meta:resourcekey="nationaltyResource1"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="label">
                            E-mail :
                        </td>
                        <td>
                            <asp:TextBox ID="email" runat="server" meta:resourcekey="emailResource1"></asp:TextBox><span class="frmStar"> *</span>
                        </td>
                        <td class="label">
                            Religion :
                        </td>
                        <td>
                            <asp:TextBox ID="religion" runat="server" meta:resourcekey="religionResource1"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="label">
                            Mobile :
                        </td>
                        <td>
                            <asp:TextBox ID="mobile" runat="server" meta:resourcekey="mobileResource1"></asp:TextBox><span class="frmStar"> *</span>
                        </td>
                        <td class="label">
                            Race :
                        </td>
                        <td>
                            <asp:TextBox ID="race" runat="server" meta:resourcekey="raceResource1"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="label">
                            Phone :
                        </td>
                        <td>
                            <asp:TextBox ID="phone" runat="server" meta:resourcekey="phoneResource1"></asp:TextBox>
                        </td>
                        <td class="label">
                            &nbsp;ID Card :
                        </td>
                        <td>
                            <asp:TextBox ID="idCard" runat="server" meta:resourcekey="idCardResource1"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" valign="top">
                            Military :
                        </td>
                        <td colspan="3">
                            <asp:RadioButtonList ID="RadioButtonList3" runat="server" Width="229px" 
                                RepeatDirection="Horizontal" meta:resourcekey="RadioButtonList3Resource1">
                                <asp:ListItem Selected="True" meta:resourcekey="ListItemResource13">Completed</asp:ListItem>
                                <asp:ListItem meta:resourcekey="ListItemResource14">No Completed</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" valign="top">
                            Address :
                        </td>
                        <td colspan="3">
                            <asp:TextBox ID="address" runat="server" Rows="2" 
                                TextMode="MultiLine" meta:resourcekey="addressResource1"></asp:TextBox>
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </div>
            <!-- end class="row-section"  -->
            <div class="row-section">
                <h2>
                    Education Education Education<span style="font-size: 12px"> (Level/institution/GPA.)</span></h2>
                <table border="0px">
                    <tr>
                        <td class="label" valign="top">
                            Vocational :
                        </td>
                        <td>
                            <asp:TextBox ID="eduVocational" runat="server" Rows="2" 
                                TextMode="MultiLine" meta:resourcekey="eduVocationalResource1" 
                                Wrap="False"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" class="label">
                            Diploma :
                        </td>
                        <td>
                            <asp:TextBox ID="edoDiploma" runat="server" Rows="2" 
                                TextMode="MultiLine" meta:resourcekey="edoDiplomaResource1"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="label">
                            Bachelor :
                        </td>
                        <td>
                            <asp:TextBox ID="eduBachelor" runat="server" Rows="2" 
                                TextMode="MultiLine" meta:resourcekey="eduBachelorResource1"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="label">
                            Master :
                        </td>
                        <td>
                            <asp:TextBox ID="eduMaster" runat="server" Rows="2" 
                                TextMode="MultiLine" meta:resourcekey="eduMasterResource1"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </div>
            <!-- end class="row-section"  -->
        </div>
        <!--end class="colLeft" --->
        <div class="colRight">
            <div class="row-section">
                <h2>
                    Attach File<span style="font-size: 12px"> ( pdf , doc , docx )</span></h2>
                <table border="0px">
                    <tr>
                        <td class="label" valign="top">
                            File :
                        </td>
                        <td>
                            <asp:FileUpload ID="fileUpload" runat="server" 
                                meta:resourcekey="fileUploadResource1" />
                        </td>
                    </tr>
                </table>
            </div>
            <!-- end class="row-section"  -->
            <div class="row-section">
                <h2>
                    Training Couse<span style="font-size: 12px"> (Date/Couse/Institution/Period)</span></h2>
                <table border="0px">
                    <tr>
                        <td class="label" valign="top">
                            <span style="text-align: right;">Training : </span>
                        </td>
                        <td>
                            <asp:TextBox ID="training" runat="server" TextMode="MultiLine" Rows="2" 
                                meta:resourcekey="trainingResource1"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </div>
            <!-- end class="row-section"  -->
            <div class="row-section">
                <h2>
                    Employment History<span style="font-size: 12px"> (Date/Company/Position/Reason of leaving)</span></h2>
                <table border="0px">
                    <tr>
                        <td class="label" valign="top">
                            <span style="text-align: right;">Recent Job 1 : </span>
                        </td>
                        <td>
                            <asp:TextBox ID="recentJob1" runat="server" TextMode="MultiLine" Rows="2" 
                                meta:resourcekey="recentJob1Resource1"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" valign="top">
                            <span style="text-align: right;">Recent Job 2 : </span>
                        </td>
                        <td>
                            <asp:TextBox ID="recentJob2" runat="server" TextMode="MultiLine" Rows="2" 
                                meta:resourcekey="recentJob2Resource1"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" valign="top">
                            <span style="text-align: right;">Recent Job 3 : </span>
                        </td>
                        <td>
                            <asp:TextBox ID="recentJob3" runat="server" TextMode="MultiLine" Rows="2" 
                                meta:resourcekey="recentJob3Resource1"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </div>
            <!-- end class="row-section"  -->
            <div class="row-section">
                <h2>
                    Computer Skill</h2>
                <h3>
                    Programing</h3>
                <asp:CheckBoxList ID="CblComSkill" runat="server" RepeatColumns="4" 
                RepeatDirection="Horizontal"  Style="margin-right: 0px" Width="448px" 
                 onselectedindexchanged="CblComSkill_SelectedIndexChanged"     
                    AutoPostBack=True
                    
                    
                    meta:resourcekey="CblComSkillResource1">
                    <asp:ListItem meta:resourcekey="ListItemResource15">ASP.NET</asp:ListItem>
                    <asp:ListItem meta:resourcekey="ListItemResource16">C#.NET</asp:ListItem>
                    <asp:ListItem meta:resourcekey="ListItemResource17">C/C++</asp:ListItem>
                    <asp:ListItem meta:resourcekey="ListItemResource18">VB.NET</asp:ListItem>
                    <asp:ListItem meta:resourcekey="ListItemResource19">VB6</asp:ListItem>
                    <asp:ListItem meta:resourcekey="ListItemResource20">ASP</asp:ListItem>
                    <asp:ListItem meta:resourcekey="ListItemResource21">PHP</asp:ListItem>
                    <asp:ListItem meta:resourcekey="ListItemResource22">Delphi</asp:ListItem>
                    <asp:ListItem meta:resourcekey="ListItemResource23">J2SE</asp:ListItem>
                    <asp:ListItem meta:resourcekey="ListItemResource24">J2ME</asp:ListItem>
                    <asp:ListItem meta:resourcekey="ListItemResource25">COBAL</asp:ListItem>
                    <asp:ListItem meta:resourcekey="ListItemResource26">AS400</asp:ListItem>
                    <asp:ListItem meta:resourcekey="ListItemResource27">ASM</asp:ListItem>
                    <asp:ListItem meta:resourcekey="ListItemResource28">HTML</asp:ListItem>
                    <asp:ListItem meta:resourcekey="ListItemResource29">RPG</asp:ListItem>
                </asp:CheckBoxList>
                <h3>
                    OS</h3>
                <asp:CheckBoxList ID="CblOs" runat="server" RepeatColumns="3" RepeatDirection="Horizontal"
                    Style="margin-right: 0px" Width="400px"  AutoPostBack=true
                    meta:resourcekey="CblOsResource1" 
                    onselectedindexchanged="CblOs_SelectedIndexChanged">
                    <asp:ListItem meta:resourcekey="ListItemResource30">Windows</asp:ListItem>
                    <asp:ListItem meta:resourcekey="ListItemResource31">Unix</asp:ListItem>
                    <asp:ListItem meta:resourcekey="ListItemResource32">Linux</asp:ListItem>
                </asp:CheckBoxList>
                <h3>
                    Database</h3>
                <asp:CheckBoxList ID="CblDatabase" runat="server" RepeatColumns="4" RepeatDirection="Horizontal"
                    Style="margin-right: 0px" Width="400px" AutoPostBack=true 
                    meta:resourcekey="CblDatabaseResource1" 
                    onselectedindexchanged="CblDatabase_SelectedIndexChanged"  >
                    <asp:ListItem meta:resourcekey="ListItemResource33">SQL Server</asp:ListItem>
                    <asp:ListItem meta:resourcekey="ListItemResource34">MYSQL Server</asp:ListItem>
                    <asp:ListItem meta:resourcekey="ListItemResource35">Oracle</asp:ListItem>
                    <asp:ListItem meta:resourcekey="ListItemResource36">Access</asp:ListItem>
                </asp:CheckBoxList>
            </div>
            <!-- end class="row-section"  -->
            <div class="row-section">
                <h2>
                    Specail Abilities</h2>
                <table width="450px" class='position-required'>
                    <tr>
                        <td colspan="2">
                            <h3>
                                English</h3>
                    </tr>
                    <tr>
                        <td class="style1">
                            Speak
                        </td>
                        <td style="width: 40%">
                            <asp:CheckBoxList ID="CblEngSpeak" runat="server" RepeatColumns="3" RepeatDirection="Horizontal"
                                Width="300px" meta:resourcekey="CblEngSpeakResource1" 
                   AutoPostBack=true              onselectedindexchanged="CblEngSpeak_SelectedIndexChanged">
                                <asp:ListItem meta:resourcekey="ListItemResource37">Excellent</asp:ListItem>
                                <asp:ListItem meta:resourcekey="ListItemResource38">Fair</asp:ListItem>
                                <asp:ListItem meta:resourcekey="ListItemResource39">Poor</asp:ListItem>
                            </asp:CheckBoxList>
                        </td>
                    </tr>
                    <tr>
                        <td class="style1">
                            Writing
                        </td>
                        <td>
                            <asp:CheckBoxList ID="CblEngWrite" runat="server" RepeatColumns="3" RepeatDirection="Horizontal" AutoPostBack=true 
                                Width="300px" meta:resourcekey="CblEngWriteResource1" 
                                onselectedindexchanged="CblEngWrite_SelectedIndexChanged">
                                <asp:ListItem meta:resourcekey="ListItemResource40">Excellent</asp:ListItem>
                                <asp:ListItem meta:resourcekey="ListItemResource41">Fair</asp:ListItem>
                                <asp:ListItem meta:resourcekey="ListItemResource42">Poor</asp:ListItem>
                            </asp:CheckBoxList>
                        </td>
                    </tr>
                    <tr>
                        <td class="style1">
                            <h3>
                                Ability to drive</h3>
                        </td>
                        <td width="20%">
                            <asp:CheckBoxList ID="CblAbiDrive" runat="server" RepeatColumns="3" RepeatDirection="Horizontal"  AutoPostBack=true 
                                Width="200px" meta:resourcekey="CblAbiDriveResource1" 
                                onselectedindexchanged="CblAbiDrive_SelectedIndexChanged">
                                <asp:ListItem meta:resourcekey="ListItemResource43">Car</asp:ListItem>
                                <asp:ListItem meta:resourcekey="ListItemResource44">Motorcycle</asp:ListItem>
                            </asp:CheckBoxList>
                        </td>
                    </tr>
                    <tr>
                        <td class="style1">
                            <h3>
                                Owner Car</h3>
                        </td>
                        <td width="20%">
                            <asp:CheckBoxList ID="CblOwnCar" runat="server" RepeatColumns="3" 
                                RepeatDirection="Horizontal"  AutoPostBack=true 
                                Width="200px" meta:resourcekey="CblOwnCarResource1" 
                                onselectedindexchanged="CblOwnCar_SelectedIndexChanged">
                                <asp:ListItem meta:resourcekey="ListItemResource45">Car</asp:ListItem>
                                <asp:ListItem meta:resourcekey="ListItemResource46">Motorcycle</asp:ListItem>
                            </asp:CheckBoxList>
                        </td>
                    </tr>
                    <tr>
                        <td class="style1">
                            <h3>
                                Driving license</h3>
                        </td>
                        <td width="20%">
                            <asp:CheckBoxList ID="CblDriLicen" runat="server" RepeatColumns="3" RepeatDirection="Horizontal"  AutoPostBack=true 
                                Width="200px" meta:resourcekey="CblDriLicenResource1" 
                                onselectedindexchanged="CblDriLicen_SelectedIndexChanged">
                                <asp:ListItem meta:resourcekey="ListItemResource47">Car</asp:ListItem>
                                <asp:ListItem meta:resourcekey="ListItemResource48">Motorcycle</asp:ListItem>
                            </asp:CheckBoxList>
                        </td>
                    </tr>
                    <tr>
                        <td class="style1">
                            &nbsp;
                        </td>
                        <td width="20%">
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </div>
            <!-- end class="row-section"  -->
        </div>
        <!--end class="colRight" --->
        <div class="clear">
        </div>
        <div class="formBtn">
            <asp:Button ID="Button1" runat="server" Text="SUBMIT" class="submit" OnClick="Button1_Click"
                OnClientClick="return chkNull();" meta:resourcekey="Button1Resource1" />
            <asp:Button ID="Button2" runat="server" Text="RESET" class="reset" 
                meta:resourcekey="Button2Resource1" />
        </div>
        <!--end class="formBtn" --->
    </div>
    <!--end id="page-apply" --->
    </form>
</asp:Content>
