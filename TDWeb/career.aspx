﻿<%@ Page Language="C#" MasterPageFile="~/MasterPageFront1.master" AutoEventWireup="true" CodeFile="career.aspx.cs" Inherits="tdSoft.career" Title="Untitled Page" %>

 
    
    
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content> 
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div id="sideLeft">
      <h1>  Position</h1>
      <div id="switchContent">
        <ul>
          <li><a href="#displayContent1">RPG/AS400 Programmer</a></li>
          <li><a href="#displayContent2">Application Developer (C#.Net)</a></li>
          <li><a href="#displayContent3">Java Programmer</a></li>
          <li><a href="#displayContent4">PL/SQL Programmer</a></li>
          <li><a href="#displayContent5">Software Tester</a></li>
        </ul>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
      </div>
    </div>
    <div id="sideRight">
    <div style="margin :10px 0px 20px 0px; color:#27d60a;"><strong> Qualification : </strong></div> 

      <div id="displayContent1" > 
        <ul>
          <li>Bachelor Degree or higher in Computer Science, Computer Engineer,
            Telecommunications or related field. </li>
          <li>At least 2-5 years in RPG/400, RPGLE/400, Subfile & OS/400 platform</li>
          <li>Able to work at customer site.</li>
          <li>Male or Female & Thai nationality only</li>
        </ul>
      </div>
      <div id="displayContent2"  >  
        <ul>
          <li>Male / Female, Thai Nationality </li>
          <li>Bachelor Degree or higher in Computer Science, Computer Engineering or related field</li>
          <li>At least 3+ years experiences in development by using MS.NET (C#, ASP .Net, AJAX, JAVA Script, VB.Net)</li>
          <li>Good knowledge in MS SQL Server & Oracle Database.</li>
          <li>Good analysis and problem solving skill</li>
          <li>Able to work at customer site.</li>
        </ul>
      </div>
      <div id="displayContent3"  >  
        <ul>
          <li>Bachelor Degree or higher in Computer Science, Computer Engineer, Telecommunications or related field.</li>
          <li>At least 2 years in Java, J2EE Programming.</li>
          <li>Male or Female age not over 35 years old & Thai nationality only</li>
          <li>Able to work at customer site.</li>
        </ul>
      </div>
      <div id="displayContent4"   >  
        <ul>
          <li>Bachelor Degree or higher in Computer Science, Computer Engineer, Telecommunications or related field.</li>
          <li>At least 2-5 years experience in Oracle PL/SQL and/or VB programming</li>
          <li>Storng in Oracle DB</li>
          <li>Have experience in Banking background or Telcom to be advantage.</li>
          <li>Experience in Unix , OS Command, Shell Command to be advantage (optional)</li>
          <li>Able to work at customer site & start work immediately to be advantage.</li>
        </ul>
      </div>
      <div id="displayContent5"  >  
        <ul>
          <li>Bachelor’s Degree in Information Technology, Computer Science or related fields</li>
          <li>At least 2 years experience in Software Tester</li>
          <li>Have knowledge in PL/SQL & VB programing (3 positions)</li>
          <li>Able to work at customer site.</li>
          <li>Male or Female Thai nationality only.</li>
        </ul>
      </div>
    </div>
    
    
    
</asp:Content>
