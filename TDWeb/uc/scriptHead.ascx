﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="scriptHead.ascx.cs" Inherits="uc_navTop" %>

<!-------- default script ---------->
<link rel="stylesheet" type="text/css"  href="<%=ConfigPath.SiteUrl %>css/global.css"  /> 
<script type="text/javascript" src="<%=ConfigPath.SiteUrl %>scripts/jquery-1.4.1.js"></script>  
<script type="text/javascript"   src="<%=ConfigPath.SiteUrl %>js/base.js"></script> 
<!-------- lavaLamp-menu ---------->
<link rel="stylesheet" type="text/css" href="<%=ConfigPath.SiteUrl %>js/lavaLamp-menu/lavaLamp.css" /> 
<script type="text/javascript" src="<%=ConfigPath.SiteUrl %>js/lavaLamp-menu/jquery.easing.1.3.js"></script> 
<script type="text/javascript" src="<%=ConfigPath.SiteUrl %>js/lavaLamp-menu/lavaLamp.js"></script>

<!-------- fadIn Content----------> 
<script type="text/javascript" src="<%=ConfigPath.SiteUrl %>js/switchContent.js"> </script>

<!-------- Modal Window ---------->
<link type="text/css" rel="stylesheet" href="<%=ConfigPath.SiteUrl %>js/modalWindow/modalWindow.css" /> 
<script type="text/javascript" src="<%=ConfigPath.SiteUrl %>js/modalWindow/modalWindow.js"> </script>