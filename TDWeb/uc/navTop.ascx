﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="navTop.ascx.cs" Inherits="uc_navTop" %>
 

<div id="lava">
    <ul>
         <li class="<% var setSelect1 = Request.Url.AbsolutePath == "/home.aspx" ? "selected" : ""; Response.Write(setSelect1);  %>"><a href="home.aspx">COMPANY</a></li> 
        <li class="<% var setSelect2 = Request.Url.AbsolutePath == "/about.aspx" ? "selected" : ""; Response.Write(setSelect2);  %>"><a href="about.aspx">ABOUT US</a></li>
         <li class="<% var setSelect3 = Request.Url.AbsolutePath == "/service.aspx" ? "selected" : ""; Response.Write(setSelect3);  %>"><a href="service.aspx">SERVICE</a></li>
         <li class="<% var setSelect4 = Request.Url.AbsolutePath == "/career.aspx" ? "selected" : ""; Response.Write(setSelect4);  %>"><a href="career.aspx">CAREER</a></li>
         <li class="<% var setSelect5 = Request.Url.AbsolutePath == "/contact.aspx" ? "selected" : ""; Response.Write(setSelect5);  %>"><a href="contact.aspx">CONTACT</a></li>
    </ul>
    <!-- If you want to make it even simpler, you can append these html using jquery -->
    <div id="box">
        <!--<div class="head" style="width: 43px; display: block; "></div>-->
    </div>
</div>
<!--  end  id="lava" -->
