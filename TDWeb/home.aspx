﻿<%@ Page Language="C#" MasterPageFile="~/MasterPageFront1.master" AutoEventWireup="true" CodeFile="home.aspx.cs"
    Inherits="tdSoft.WebForm3" Title="Untitled Page" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <title>T.D. Software Co.,Ltd. - IT Company | IT Outsourcing | Outsource Programming
        | Web Application | Software Development | eCommerce website</title>
  
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="sideLeft">
        <h1>Company</h1> 
        <h2>Why we call our group as T.D.Software</h2>
        <p class="content">
            T.D. abbreviate from Three Dimension of Requirement Engineering: RE (Specification,
            Agreement and Representation). It is requirement engineering model that support
            RE process for the good quality of software product. We develop solution by practice
            and apply this software engineering process as our standardization then we name
            us and our company.
        </p>
    </div>
    <div id="sideRight">
        <img src="images/home-_06.jpg" width="443" height="333" />
    </div>
</asp:Content>
