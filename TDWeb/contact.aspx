﻿<%@ Page Language="C#" MasterPageFile="~/MasterPageFront1.master" AutoEventWireup="true" CodeFile="contact.aspx.cs" Inherits="tdSoft.contact" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"> 

<title>T.D. Software Co.,Ltd. - IT Company | IT Outsourcing | Outsource Programming | Web Application | Software Development | eCommerce website</title>
<meta name="keywords" content="tdsoftware, td software, tdsoftware.co.th, outsource, Outsource Programming , developer , programmer, ASP.Net programmer, .net  programmer , java programmer ,rpg programmer, c#  programmer, vb  programmer, iPhone programmer ,  microsoft programmer , sql server , .net  developer, java developer ,rpg developer, c#  developer, vb  developer, ecommerce developer, outsource employee,  
IT Company , IT outsourcing ,  Web Application , Software Development , iPhone Development ,eCommerce website ,
hire programmer,   website programmer, website developer,  iphone programmer,  application programmer, programming services , Thai Programmer
" />
<meta name="description" 
content="Provide programmer for every business. To develop applications ASP.Net programmer, Java programmers, eCommerce website developer, iPhone programmer, programming services." />
 
 
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="sideLeft">
      <h1>Contact</h1> 
      <p> <a  href="#dialog"  name="modal"><img src="images/mapSmall.jpg" width="447" height="170" /></a></p>
      <p>&nbsp; </p>
      <p>E-mail : <strong>admin@tdsoftware.co.th </strong><br />
        Tel : <strong>0-2634-4426</strong>  &nbsp;&nbsp;&nbsp;Fax : <strong>0-2634-4427</strong></p>
      <p>T.D. Software Co., Ltd. 88 PASO Tower, 27th Fl., Silom Rd., <br />
        Suriyawongse, Bangrak, Bangkok 10500 </p>
      <h2>&nbsp;</h2>
    </div>
    
    
    
    <!-- Start modalWindow -->  
<div id="boxes">
  <div id="dialog" class="window"><img src="images/mapBig.jpg" />
  <a href="#"class="close"/><img src="js/modalWindow/btClose.png"  /></a>
  </div>
 <!-- Mask to cover the whole screen -->
  <div id="mask"></div>
</div>
<!-- End modalWindow -->
</asp:Content>
