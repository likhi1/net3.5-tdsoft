﻿<%@ page language="C#" autoeventwireup="true" inherits="Login, App_Web_login.aspx.cdcab7d2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ยินดีต้อนรับเข้าสู่ระบบการลงทุน</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link rel="stylesheet" type="text/css" href=/<%=strAppName%>/StyleSheets/fontface1.css media="screen" />
    <link rel="stylesheet" type="text/css" href=/<%=strAppName%>/StyleSheets/loginViriya.css media="screen" />
</head>

<body onKeyPress="callSubmit()">
    <form id="form1" runat="server">
        <script language="javascript">
        function goPage(strPage)
        {
            window.location= "/<%=strAppName%>/" + strPage;
            return false;
        }
        function validateForm()
        {    
            var isRet = true;
            var strMsg = "";
            var strFocus = "";
            
            if (document.getElementById("<%=txtUserName.ClientID %>").value == ""){
                isRet = false;
                strMsg += "    - ชื่อผู้ใช้งาน\n";
                if (strFocus == "")
                    strFocus = document.getElementById("<%=txtUserName.ClientID %>");
            }
            if (document.getElementById("<%=txtPassword.ClientID %>").value == ""){
                isRet = false;
                strMsg += "    - รหัสผ่าน\n";
                if (strFocus == "")
                    strFocus = document.getElementById("<%=txtPassword.ClientID %>");
            }
            
            
            if (isRet == false){
                alert("! กรุณาระบุข้อมูล : \n"+ strMsg);
                if (strFocus != "")
                    eval(strFocus.focus());
            }
                
            return isRet ;
        }
        function validateChangePassword()
        {
            var isRet = true;
            var strMsg = "";
            if (document.getElementById("<%=txtNewPassword.ClientID %>").value == ""){
                isRet = false;
                strMsg += "\t-  รหัสผ่านใหม่\n";
            }
            if (document.getElementById("<%=txtRenewPassword.ClientID %>").value == ""){
                isRet = false;
                strMsg += "\t-  ยืนยันรหัสผ่านใหม่\n";
            }            
            if (document.getElementById("<%=txtNewPassword.ClientID %>").value != document.getElementById("<%=txtRenewPassword.ClientID %>").value)
            {
                isRet = false;
                strMsg += "! รหัสผ่านใหม่ ไม่ตรงกับ ยืนยันรหัสผ่านใหม่";
            }

            if (isRet == false)
                alert("! กรุณาระบุข้อมูล\n" + strMsg);            
            return isRet;
        }
        function callSubmit()
        {
            if (event.keyCode == 13) {
                if (validateForm()) __doPostBack('<%=btnLogin.ClientID %>','');
            }
        }
    </script>
    
  

	<!-- Start of Main Content -->
	
<div class="container-outer">
<div class="container">
<div class="header">
<div class="logo"></div>
</div>
<div class="content">

<div class="loginTitle">
<h1>ระบบการลงทุน</h1>
<h2>กรุณา Login เพื่อเข้าสู่ระบบ</h2> 
</div><!--end  class="loginTitle"--->


<div class="loginArea">

<p><label>User :</label>
<asp:TextBox ID="txtUserName" runat="server" Width="100"></asp:TextBox> 
</p>

<p>
<label>Password :</label>
<asp:TextBox ID="txtPassword" runat="server" TextMode="Password" Width="100"></asp:TextBox> 
</p>

<p class="btnLogin"> 
<asp:LinkButton ID="btnLogin" runat="server" OnClick="btnLogin_Click" OnClientClick="return validateForm();">เข้าสู่ระบบ</asp:LinkButton>

<asp:LinkButton ID="btnClear" runat="server" OnClientClick="return goPage('Login.aspx');">เคลียร์</asp:LinkButton>
</p>


</div><!--- end class="areaLogin" -->

</div><!--- end class="content" -->
 
<div class="footer">
Copyright © 2012 Viriyah Insurance, Thailand Co., Ltd. &nbsp; - &nbsp; Powered by T.D. Software 
</div><!-- end class="footer" -->


</div><!-- end class="container" --> 
<div id="divHeight"  style="height:50px"></div>
</div>
<script>    
    document.getElementById("divHeight").style.height = window.screen.height - 750;
</script>
   

	<!-- End of Main Content -->

 
  

 
    <div style="position:absolute; display:none; top:380px; left:490px; z-index:2; width:350px ; 
        background-color: #144f85 ; color:#fff; border: 2px solid #d0d4d7 ; " id="divChangePassword">
    <table class="bgPopupLayer" width="100%">
        <tr>
            <td>&nbsp;</td>
             <td>&nbsp;</td>
        </tr>
        <tr>
            
            <td  width=50% align="right">รหัสผ่านใหม่ :</td>
            <td>
                <asp:TextBox ID="txtNewPassword" runat="server" CssClass="TEXTBOX" Width="100" MaxLength="13" TextMode="Password"></asp:TextBox>
            </td>
             
        </tr>
        <tr>
           
            <td align="right">ยืนยันรหัสผ่านใหม่ :</td>
            <td>
                <asp:TextBox ID="txtRenewPassword" runat="server" CssClass="TEXTBOX" Width="100" MaxLength="13" TextMode="Password"></asp:TextBox>
            </td>
            
        </tr>
        <tr>
            <td></td>        
            <td  >
                <asp:Button ID="btnConfirmChangePwd" runat="server" Text="ตกลง" 
                    CssClass="BUTTON" OnClientClick="return validateChangePassword();" 
                    onclick="btnConfirmChangePwd_Click"/>
                <input id="Button1" name="btnClear" type="button" value="ยกเลิก" class="BUTTON" onclick="document.getElementById('divChangePassword').style.display = 'none';" />
            </td>
            
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
    </table>
    </div>
    
    
    <asp:Literal ID="litScript" runat="server"></asp:Literal>
    </form>
</body>
</html>
