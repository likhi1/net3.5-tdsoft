﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//----Increase NameSpace -------------------//
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Configuration;
using System.Web.Security;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.Collections; //for ArrayList


namespace tdSoft {

    // http://www.thaicreate.com/asp.net/c-sharp-asp.net-user-register-form.html

    public partial class apply : System.Web.UI.Page {

        protected void Page_Load(object sender, EventArgs e) {

            if (!Page.IsPostBack) {
                DdlAge_AddList(null, null);
                DdlBrtDay_AddList(null, null);
                DdlBrtMonth_AddList(null, null);
                DdlBrtYear_AddList(null, null);
                CblPosition_SelectedIndexChanged(null, null);
                CblComSkill_SelectedIndexChanged(null, null);
                CblOs_SelectedIndexChanged(null, null);
            }
            else {

            }

        }

        //----- DropDown  Select --------------------------------------------------------------------------//

        protected void DdlAge_AddList(object sender, EventArgs e) {
            ArrayList _DdlAge = new ArrayList();
            for (int i = 20; i <= 60; i++) {
                _DdlAge.Add(i);
            }
            DdlAge.DataSource = _DdlAge;
            DdlAge.DataBind();   // bind data 
            DdlAge.Items.Insert(0, "Age");    // add Empty Item
        }

        protected void DdlBrtDay_AddList(object sender, EventArgs e) {
            ArrayList _DdlBrtDay = new ArrayList();
            for (int i = 1; i <= 31; i++) {
                if (i < 10) {
                    _DdlBrtDay.Add("0" + i);
                }
                else {
                    _DdlBrtDay.Add(i);
                }

            }
            DdlBrtDay.DataSource = _DdlBrtDay;
            DdlBrtDay.DataBind();   // bind data 
            DdlBrtDay.Items.Insert(0, new ListItem("Day", ""));   // add Empty Item
        }

        protected void DdlBrtMonth_AddList(object sender, EventArgs e) {
            ArrayList _DdlBrtMonth = new ArrayList();
            for (int i = 1; i <= 12; i++) {
                if (i < 10) {
                    _DdlBrtMonth.Add("0" + i);
                }
                else {
                    _DdlBrtMonth.Add(i);
                }
            }
            DdlBrtMonth.DataSource = _DdlBrtMonth;
            DdlBrtMonth.DataBind();   // bind data 
            DdlBrtMonth.Items.Insert(0, new ListItem("Month", ""));    // add Empty Item

        }

        protected void DdlBrtYear_AddList(object sender, EventArgs e) {
            ArrayList _DdlBrtYear = new ArrayList();
            for (int i = 2013; i >= 1952; i--) {
                _DdlBrtYear.Add(i);
            }
            DdlBrtYear.DataSource = _DdlBrtYear;
            DdlBrtYear.DataBind();   // bind data 
            DdlBrtYear.Items.Insert(0, new ListItem("Year", ""));    // add Empty Item
        }


        //----- Checkbox Check --------------------------------------------------------------------------//

        protected void CblPosition_SelectedIndexChanged(object sender, EventArgs e) {
            ArrayList _CblPosition = new ArrayList();
            foreach (ListItem nItem in CblPosition.Items) {
                if (nItem.Selected) {
                    _CblPosition.Add(nItem.Text);
                    Response.Write(nItem.Text);
                }
            }// end foreach  
        }

        protected void CblComSkill_SelectedIndexChanged(object sender, EventArgs e) {
            ArrayList _CblComSkill = new ArrayList();
            foreach (ListItem nItem in CblComSkill.Items) {
                if (nItem.Selected) {
                    _CblComSkill.Add(nItem.Text);
                    Response.Write(nItem.Text);
                }
            }// end foreach  
        }

        protected void CblOs_SelectedIndexChanged(object sender, EventArgs e) {
            ArrayList _CblOs = new ArrayList();
            foreach (ListItem nItem in CblOs.Items) {
                if (nItem.Selected) {
                    _CblOs.Add(nItem.Text);
                    Response.Write(nItem.Text);
                }
            }// end foreach  
        }

        protected void CblDatabase_SelectedIndexChanged(object sender, EventArgs e) {
            ArrayList _CblDatabase = new ArrayList();
            foreach (ListItem nItem in CblDatabase.Items) {
                if (nItem.Selected) {
                    _CblDatabase.Add(nItem.Text);
                    Response.Write(nItem.Text);
                }
            }// end foreach  
        }

        protected void CblEngSpeak_SelectedIndexChanged(object sender, EventArgs e) {
            ArrayList _CblEngSpeak = new ArrayList();
            foreach (ListItem nItem in CblEngSpeak.Items) {
                if (nItem.Selected) {
                    _CblEngSpeak.Add(nItem.Text);
                    Response.Write(nItem.Text);
                }
            }// end foreach
        }

        protected void CblEngWrite_SelectedIndexChanged(object sender, EventArgs e) {
            ArrayList _CblEngWrite = new ArrayList();
            foreach (ListItem nItem in CblEngWrite.Items) {
                if (nItem.Selected) {
                    _CblEngWrite.Add(nItem.Text);
                    Response.Write(nItem.Text);
                }
            }// end foreach
        }

        protected void CblAbiDrive_SelectedIndexChanged(object sender, EventArgs e) {
            ArrayList _CblAbiDrive = new ArrayList();
            foreach (ListItem nItem in CblAbiDrive.Items) {
                if (nItem.Selected) {
                    _CblAbiDrive.Add(nItem.Text);
                    Response.Write(nItem.Text);
                }
            }// end foreach
        }




        protected void CblOwnCar_SelectedIndexChanged(object sender, EventArgs e) {
            ArrayList _CblOwnCar = new ArrayList();
            foreach (ListItem nItem in CblOwnCar.Items) {
                if (nItem.Selected) {
                    _CblOwnCar.Add(nItem.Text);
                    Response.Write(nItem.Text);
                }
            }// end foreach

        }


        protected void CblDriLicen_SelectedIndexChanged(object sender, EventArgs e) {
            ArrayList _CblAbiDrive = new ArrayList();
            foreach (ListItem nItem in CblAbiDrive.Items) {
                if (nItem.Selected) {
                    _CblAbiDrive.Add(nItem.Text);
                    Response.Write(nItem.Text);
                }
            }// end foreach

        }





        //----- Insert -------------------------------------------------------------------------//


        protected void Button1_Click(object sender, EventArgs e) {

            /*-- Set var --------------------------------------------------*/
            string _salary = salary.Text;
            string _positionLevel = RadioButtonList1.SelectedItem.Value; 
            string _nameF = nameF.Text;
            string _nameL = nameL.Text;
            string _gen = RadioButtonList2.SelectedItem.Value;
            string _age = DdlAge.SelectedItem.Value;
            string _email = email.Text;
            string _mobile = mobile.Text;
            string _phone = phone.Text;

            string _DdlBrtDay = DdlBrtDay.SelectedItem.Value;
            string _DdlBrtMonth = DdlBrtMonth.SelectedItem.Value;
            string _DdlBrtYear = DdlBrtYear.SelectedItem.Value;

            string _birth = _DdlBrtYear+"-"+_DdlBrtMonth+"-"+_DdlBrtDay  ;    // Formatch Date   yyyy-MM-dd HH:mm:ss



            string _RegDate = weight.Text;
            string _RegIp = Request.ServerVariables["REMOTE_ADDR"];

            string _weight = weight.Text;
            string _height = height.Text;
            string _nationalty = nationalty.Text;
            string _religion = religion.Text;
            string _race = race.Text;
            string _idCard = idCard.Text; 
            string _military = RadioButtonList3.SelectedItem.Value;
            string _address = address.Text; 
            string _eduVoc = eduVocational.Text;
            string _edoDip = edoDiploma.Text;
            string _eduBa = eduBachelor.Text;
            string _eduMas = eduMaster.Text; 
            string _fileUpload = "";
  
            string _training = training.Text;
            string _recentJob1 = recentJob1.Text;
            string _recentJob2 = recentJob2.Text;
            string _recentJob3 = recentJob3.Text;




            ArrayList _CblPosition, _CblComSkill, _CblOs, _CblDatabase, _CblEngSpeak, _CblEngWrite, _CblAbiDrive, _CblOwnCar, _CblDriLicen;



            if (_salary != "" && _nameF != "") { 


                /*-- Connect DB ----------------------------------------------------*/
                string strConn;
                string sqlTest;

                strConn = WebConfigurationManager.ConnectionStrings["myConn1"].ConnectionString; // use Web.config for connect
                SqlConnection Conn = new SqlConnection(strConn);
                Conn.Open();

                /*--bind data ---------*/
                //  tbApUser  = 22 feild
                sqlTest = "INSERT INTO  tbApUser  ";
                sqlTest += "valuses ('_salary' , '_positionLevel' , 'val3', 'val4', 'val5' , 'val6', 'val7', 'val8', 'val9' , 'val10' ,    ";
                sqlTest += " val11' , 'val12' , 'val13', 'val14', 'val15' , 'val16', 'val17', 'val18', 'val19' , val20' , 'val21' , 'val22' )";

                SqlDataAdapter da = new SqlDataAdapter(sqlTest, Conn);


            }// end if



        }


    }


}