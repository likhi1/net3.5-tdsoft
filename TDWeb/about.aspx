﻿<%@ Page Language="C#" MasterPageFile="~/MasterPageFront1.master" AutoEventWireup="true" CodeFile="about.aspx.cs" Inherits="tdWeb.about" Title="Untitled Page" %>
  
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

<title>T.D. Software Co.,Ltd. - IT Company | IT Outsourcing | Outsource Programming | Web Application | Software Development | eCommerce website</title>
<meta name="keywords" content="tdsoftware, td software, tdsoftware.co.th, outsource, Outsource Programming , developer , programmer, ASP.Net programmer, .net  programmer , java programmer ,rpg programmer, c#  programmer, vb  programmer, iPhone programmer ,  microsoft programmer , sql server , .net  developer, java developer ,rpg developer, c#  developer, vb  developer, ecommerce developer, outsource employee,  
IT Company , IT outsourcing ,  Web Application , Software Development , iPhone Development ,eCommerce website ,
hire programmer,   website programmer, website developer,  iphone programmer,  application programmer, programming services , Thai Programmer
" />
<meta name="description" 
content="Provide programmer for every business. To develop applications ASP.Net programmer, Java programmers, eCommerce website developer, iPhone programmer, programming services." />
 
 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="sideLeft">
      <h1>About Us</h1>
      
      <h2>Provide information system for every business, from individual owner through multinational organization.</h2>
<p class="content"> We are dedicated to provide professional service utilizing with qualified personnel, software methodology as Software Quality Control by standard of Software Engineering Laboratory, professional training team member and installation of environment to support our customer, design information system to solve their problems. <br />
        <br />
      Clients of T.D. Software can anticipate professional integration of hardware and software applications that meet and exceed their expectation. Walk together with our customers to cope their business against competitive edge. T.D. Software vision is to be the best IT Service solution provider in Thailand </p>
    </div>
    <div id="sideRight"><img src="images/about-_06.jpg" width="443" height="333" />
       
       
       
      
       
    </div>
</asp:Content>
